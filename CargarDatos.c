#include <stdio.h>
#include <stdlib.h>
#include "Carreras.h"

void cargarCarreras(char path[], carreras uv[]){ // Lo que hace esta funcion es cargar los archivos del txt a una lista para poder trabajar
	FILE *file;
	int i = 0;
	if((file=fopen(path,"r"))==NULL){
		printf("\n error al arbir el archivo");
		exit(0); 
	}
	else{
		printf("\nLos estudiantes cargados son: \n"); // Se guardan los datos del txt en la lista par apoder trabajarla
		while (feof(file) == 0) {
			fscanf(file,"%d \t%d \t%d \t%d \t%d \t%d \t%d \t%d \t%d \t%f \t%f \t%d \t%d" ,&uv[i].codigo, &uv[i].NEM, &uv[i].RANK, &uv[i].LENG, &uv[i].MAT, &uv[i].HIST, &uv[i].CS, &uv[i].POND, &uv[i].PSU, &uv[i].MAX, &uv[i].MIN, &uv[i].psu, &uv[i].BEA);
			
			i++;

		}
		fclose(file);
	} 
}
